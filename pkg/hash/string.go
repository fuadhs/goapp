package hash

/* Get hash value for string*/

// Md5String Get the string md5 value
func Md5String(s string) string {
	return Md5Byte([]byte(s))
}

// Sha1String Get the string sha1 value
func Sha1String(s string) string {
	return Sha1Byte([]byte(s))
}

// Sha256String Get the string sha256 value
func Sha256String(s string) string {
	return Sha256Byte([]byte(s))
}

// Sha512String Get the string sha512 value
func Sha512String(s string) string {
	return Sha512Byte([]byte(s))
}
