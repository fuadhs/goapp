module gitlab.com/fuadhs/goapp/pkg

go 1.19

require (
	github.com/coocood/freecache v1.2.3
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.8.1
	go.uber.org/zap v1.23.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)

require (
	github.com/BurntSushi/toml v1.2.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
