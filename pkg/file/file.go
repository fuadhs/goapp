package file

import (
	"io/ioutil"
)

// Get the file name of each file in the folder
func GetFolderSubFileName(path string) (fileNames []string, err error) {
	dirList, err := ioutil.ReadDir(path)
	if err != nil {
		return
	}
	for _, v := range dirList {
		fileNames = append(fileNames, v.Name())
	}
	return
}
