package middleware

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

// NoMethodHandler The handler function for the request method was not found
func NoMethodHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.JSON(405, gin.H{"message": "method not allowed"})
	}
}

// NoRouteHandler The handler function for the requested route was not found
func NoRouteHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.JSON(500, gin.H{"message": "the handler function for the requested route was not found"})
	}
}

// SkipperFunc Define middleware skip function
type SkipperFunc func(*gin.Context) bool

// AllowPathPrefixSkipper Check if the request path contains the specified prefix, and skip if it does
func AllowPathPrefixSkipper(prefixes ...string) SkipperFunc {
	return func(c *gin.Context) bool {
		path := c.Request.URL.Path
		pathLen := len(path)

		for _, p := range prefixes {
			if pl := len(p); pathLen >= pl && path[:pl] == p {
				return true
			}
		}
		return false
	}
}

// AllowPathPrefixNoSkipper Check if the request path contains the specified prefix, if so, do not skip
func AllowPathPrefixNoSkipper(prefixes ...string) SkipperFunc {
	return func(c *gin.Context) bool {
		path := c.Request.URL.Path
		pathLen := len(path)

		for _, p := range prefixes {
			if pl := len(p); pathLen >= pl && path[:pl] == p {
				return false
			}
		}
		return true
	}
}

// AllowMethodAndPathPrefixSkipper Check if the request method and path contain the specified prefix, and skip if it does
func AllowMethodAndPathPrefixSkipper(prefixes ...string) SkipperFunc {
	return func(c *gin.Context) bool {
		path := JoinRouter(c.Request.Method, c.Request.URL.Path)
		pathLen := len(path)

		for _, p := range prefixes {
			if pl := len(p); pathLen >= pl && path[:pl] == p {
				return true
			}
		}
		return false
	}
}

// JoinRouter splice routing
func JoinRouter(method, path string) string {
	if len(path) > 0 && path[0] != '/' {
		path = "/" + path
	}
	return fmt.Sprintf("%s%s", strings.ToUpper(method), path)
}
