package common

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

const (
	SUCCESS_CODE          = 20000      // successful status code
	FAIL_CODE             = 30000      // failed status code
	MD5_PREFIX            = "jkfldfsf" // MD5 encrypted prefix string
	TOKEN_KEY             = "X-Token"  // Page token key name
	USER_ID_Key           = "X-USERID" // Page User ID key name
	USER_UUID_Key         = "X-UUID"   // Page UUID key name
	SUPER_ADMIN_ID uint64 = 956986     // Super administrator account ID
)

type ResponseModel struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type ResponseModelBase struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// successful response
func ResSuccess(c *gin.Context, v interface{}) {
	ret := ResponseModel{Code: SUCCESS_CODE, Message: "ok", Data: v}
	ResJSON(c, http.StatusOK, &ret)
}

// successful response
func ResSuccessMsg(c *gin.Context) {
	ret := ResponseModelBase{Code: SUCCESS_CODE, Message: "ok"}
	ResJSON(c, http.StatusOK, &ret)
}

// Response failed
func ResFail(c *gin.Context, msg string) {
	ret := ResponseModelBase{Code: FAIL_CODE, Message: msg}
	ResJSON(c, http.StatusOK, &ret)
}

// Response failed
func ResFailCode(c *gin.Context, msg string, code int) {
	ret := ResponseModelBase{Code: code, Message: msg}
	ResJSON(c, http.StatusOK, &ret)
}

// Response JSON data
func ResJSON(c *gin.Context, status int, v interface{}) {
	c.JSON(status, v)
	c.Abort()
}

// Response error - server failure
func ResErrSrv(c *gin.Context, err error) {
	ret := ResponseModelBase{Code: FAIL_CODE, Message: "server failure"}
	ResJSON(c, http.StatusOK, &ret)
}

// Response error - client failure
func ResErrCli(c *gin.Context, err error) {
	ret := ResponseModelBase{Code: FAIL_CODE, Message: "err"}
	ResJSON(c, http.StatusOK, &ret)
}

type ResponsePageData struct {
	Total uint64      `json:"total"`
	Items interface{} `json:"items"`
}

type ResponsePage struct {
	Code    int              `json:"code"`
	Message string           `json:"message"`
	Data    ResponsePageData `json:"data"`
}

// Response success - paged data
func ResSuccessPage(c *gin.Context, total uint64, list interface{}) {
	ret := ResponsePage{Code: SUCCESS_CODE, Message: "ok", Data: ResponsePageData{Total: total, Items: list}}
	ResJSON(c, http.StatusOK, &ret)
}

// get page number
func GetPageIndex(c *gin.Context) uint64 {
	return GetQueryToUint64(c, "page", 1)
}

// Get the number of records per page
func GetPageLimit(c *gin.Context) uint64 {
	limit := GetQueryToUint64(c, "limit", 20)
	if limit > 500 {
		limit = 20
	}
	return limit
}

// Get sorting information
func GetPageSort(c *gin.Context) string {
	return GetQueryToStr(c, "sort")
}

// Get search keyword information
func GetPageKey(c *gin.Context) string {
	return GetQueryToStr(c, "key")
}
