package master

import (
	"gitlab.com/fuadhs/goapp/internal/app/manageweb/controllers/common"
	"gitlab.com/fuadhs/goapp/internal/app/manageweb/controllers/sys"
	models "gitlab.com/fuadhs/goapp/internal/pkg/models/common"
	"gitlab.com/fuadhs/goapp/internal/pkg/models/mast"
	"gitlab.com/fuadhs/goapp/pkg/convert"

	"github.com/gin-gonic/gin"
)

type Jenis struct{}

// Paged data
func (Jenis) List(c *gin.Context) {
	page := common.GetPageIndex(c)
	limit := common.GetPageLimit(c)
	sort := common.GetPageSort(c)
	key := common.GetPageKey(c)
	menuType := common.GetQueryToUint(c, "type")
	parent_id := common.GetQueryToUint64(c, "parent_id")
	var whereOrder []models.PageWhereOrder
	order := "ID DESC"
	if len(sort) >= 2 {
		orderType := sort[0:1]
		order = sort[1:len(sort)]
		if orderType == "+" {
			order += " ASC"
		} else {
			order += " DESC"
		}
	}
	whereOrder = append(whereOrder, models.PageWhereOrder{Order: order})
	if key != "" {
		v := "%" + key + "%"
		var arr []interface{}
		arr = append(arr, v)
		arr = append(arr, v)
		whereOrder = append(whereOrder, models.PageWhereOrder{Where: "name like ? or code like ?", Value: arr})
	}
	if menuType > 0 {
		var arr []interface{}
		arr = append(arr, menuType)
		whereOrder = append(whereOrder, models.PageWhereOrder{Where: "menu_type = ?", Value: arr})
	}
	if parent_id > 0 {
		var arr []interface{}
		arr = append(arr, parent_id)
		whereOrder = append(whereOrder, models.PageWhereOrder{Where: "parent_id = ?", Value: arr})
	}
	var total uint64
	list := []sys.Menu{}
	err := models.GetPage(&sys.Menu{}, &sys.Menu{}, &list, page, limit, &total, whereOrder...)
	if err != nil {
		common.ResErrSrv(c, err)
		return
	}
	common.ResSuccessPage(c, total, &list)
}

// details
func (Jenis) Detail(c *gin.Context) {
	id := common.GetQueryToUint64(c, "id")
	var jenis mast.JenisSurat
	where := mast.JenisSurat{}
	where.ID = id
	_, err := models.First(&where, &jenis)
	if err != nil {
		common.ResErrSrv(c, err)
		return
	}
	common.ResSuccess(c, &jenis)
}

// renew
func (Jenis) Update(c *gin.Context) {
	model := sys.Menu{}
	err := c.Bind(&model)
	if err != nil {
		common.ResErrSrv(c, err)
		return
	}
	err = models.Save(&model)
	if err != nil {
		common.ResFail(c, "operation failed")
		return
	}
	common.ResSuccessMsg(c)
}

// Create
func (Jenis) Create(c *gin.Context) {
	jenis := mast.JenisSurat{}
	err := c.Bind(&jenis)
	if err != nil {
		common.ResErrSrv(c, err)
		return
	}
	err = models.Create(&jenis)
	if err != nil {
		common.ResFail(c, "operation failed")
		return
	}
	go InitMenu(jenis)
	common.ResSuccess(c, gin.H{"id": jenis.ID})
}

// delete data
func (Jenis) Delete(c *gin.Context) {
	var ids []uint64
	err := c.Bind(&ids)
	if err != nil || len(ids) == 0 {
		common.ResErrSrv(c, err)
		return
	}
	// jenis := mast.JenisSurat{}
	// err = jenis.Delete(ids)
	// if err != nil {
	// 	common.ResErrSrv(c, err)
	// 	return
	// }
	common.ResSuccessMsg(c)
}

// all menus
func (Jenis) AllMenu(c *gin.Context) {
	var jenisx []mast.JenisSurat
	err := models.Find(&mast.JenisSurat{}, &jenisx, "parent_id asc", "sequence asc")
	if err != nil {
		common.ResErrSrv(c, err)
		return
	}
	common.ResSuccess(c, &jenisx)
}

// Automatically add routine operations under the menu after adding a new menu
func InitMenu(model mast.JenisSurat) {
	// if model.MenuType != 2 {
	// 	return
	// }
	// add := sys.Menu{Status: 1, ParentID: model.ID, URL: model.URL + "/create", Name: "Add", Sequence: 1, MenuType: 3, Code: model.Code + "Add", OperateType: "add"}
	// models.Create(&add)
	// del := sys.Menu{Status: 1, ParentID: model.ID, URL: model.URL + "/delete", Name: "Delete", Sequence: 2, MenuType: 3, Code: model.Code + "Del", OperateType: "del"}
	// models.Create(&del)
	// view := sys.Menu{Status: 1, ParentID: model.ID, URL: model.URL + "/detail", Name: "View", Sequence: 3, MenuType: 3, Code: model.Code + "View", OperateType: "view"}
	// models.Create(&view)
	// update := sys.Menu{Status: 1, ParentID: model.ID, URL: model.URL + "/update", Name: "Edit", Sequence: 4, MenuType: 3, Code: model.Code + "Update", OperateType: "update"}
	// models.Create(&update)
	// list := sys.Menu{Status: 1, ParentID: model.ID, URL: model.URL + "/list", Name: "List", Sequence: 5, MenuType: 3, Code: model.Code + "List", OperateType: "list"}
	// models.Create(&list)
}

// Get the list of operations that the menu has permission
func (Jenis) MenuButtonList(c *gin.Context) {
	// 用户ID
	uid, isExit := c.Get(common.USER_ID_Key)
	if !isExit {
		common.ResFailCode(c, "token invalid", 50008)
		return
	}
	userID := convert.ToUint64(uid)
	menuCode := common.GetQueryToStr(c, "menucode")
	if userID == 0 || menuCode == "" {
		common.ResFail(c, "err")
		return
	}
	btnList := []string{}
	if userID == common.SUPER_ADMIN_ID {
		//管理员
		btnList = append(btnList, "add")
		btnList = append(btnList, "del")
		btnList = append(btnList, "view")
		btnList = append(btnList, "update")
		btnList = append(btnList, "setrolemenu")
		btnList = append(btnList, "setadminrole")
	} else {
		// menu := sys.Menu{}
		// err := menu.GetMenuButton(userID, menuCode, &btnList)
		// if err != nil {
		// 	common.ResErrSrv(c, err)
		// 	return
		// }
	}
	common.ResSuccess(c, &btnList)
}
