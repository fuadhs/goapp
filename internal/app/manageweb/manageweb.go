package manageweb

import (
	"net/http"
	"time"

	webconfig "gitlab.com/fuadhs/goapp/internal/app/manageweb/config"
	"gitlab.com/fuadhs/goapp/internal/app/manageweb/controllers/common"
	"gitlab.com/fuadhs/goapp/internal/app/manageweb/middleware"
	"gitlab.com/fuadhs/goapp/internal/app/manageweb/routers"
	"gitlab.com/fuadhs/goapp/internal/pkg/config"
	"gitlab.com/fuadhs/goapp/internal/pkg/models"
	"gitlab.com/fuadhs/goapp/pkg/convert"
	"gitlab.com/fuadhs/goapp/pkg/logger"

	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// Run
func Run(configPath string) {
	if configPath == "" {
		configPath = "./config.yaml"
	}
	// Load configuration
	config, err := webconfig.LoadConfig(configPath)
	if err != nil {
		panic(err)
	}
	logger.InitLog("debug", "./log/logb.log")
	initDB(config)
	common.InitCsbinEnforcer()
	initWeb(config)
	logger.Debug(config.Web.Domain + " Server Started...")
}

func initDB(config *config.Config) {
	models.InitDB(config)
	models.Migration()
}

func initWeb(config *config.Config) {
	gin.SetMode(gin.DebugMode) //debug mode
	app := gin.New()
	app.NoRoute(middleware.NoRouteHandler())
	// crash recovery
	app.Use(middleware.RecoveryMiddleware())
	app.LoadHTMLGlob(config.Web.StaticPath + "dist/*.html")
	app.Static("/static", config.Web.StaticPath+"dist/static")
	app.Static("/resource", config.Web.StaticPath+"resource")
	app.StaticFile("/favicon.ico", config.Web.StaticPath+"dist/favicon.ico")
	// register route
	routers.RegisterRouter(app)
	go initHTTPServer(config, app)
}

// InitHTTPServer Initialize http service
func initHTTPServer(config *config.Config, handler http.Handler) {
	srv := &http.Server{
		Addr:         ":" + convert.ToString(config.Web.Port),
		Handler:      handler,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}
	srv.ListenAndServe()
}
