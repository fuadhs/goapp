package config

import (
	"fmt"
)

// Config configuration parameters
type Config struct {
	Web     Web
	Gorm    Gorm
	MySQL   MySQL
	Sqlite3 Sqlite3
}

// Site Configuration Parameters
type Web struct {
	Domain       string
	StaticPath       string
	Port         int
	ReadTimeout  int
	WriteTimeout int
	IdleTimeout  int
}

// Gorm gorm configuration parameters
type Gorm struct {
	Debug        bool
	DBType       string
	MaxLifetime  int
	MaxOpenConns int
	MaxIdleConns int
	TablePrefix  string
	DSN          string
}

// MySQL Parameters
type MySQL struct {
	Host       string
	Port       int
	User       string
	Password   string
	DBName     string
	Parameters string
}

// MySQL database connection string
func (a MySQL) DSN() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?%s",
		a.User, a.Password, a.Host, a.Port, a.DBName, a.Parameters)
}

// Sqlite3 Parameters
type Sqlite3 struct {
	Path string
}

// Sqlite3 database connection string
func (a Sqlite3) DSN() string {
	return a.Path
}
