package sys

import (
	"time"

	"gitlab.com/fuadhs/goapp/internal/pkg/models/basemodel"
	"gitlab.com/fuadhs/goapp/internal/pkg/models/db"

	"github.com/jinzhu/gorm"
)

// Role
type Role struct {
	basemodel.Model
	Memo     string `gorm:"column:memo;size:64;" json:"memo" form:"memo"`                 // memo
	Name     string `gorm:"column:name;size:32;not null;" json:"name" form:"name"`        // Name
	Sequence int    `gorm:"column:sequence;not null;" json:"sequence" form:"sequence"`    // sequence
	ParentID uint64 `gorm:"column:parent_id;not null;" json:"parent_id" form:"parent_id"` // Parent ID
}

// Table Name
func (Role) TableName() string {
	return TableName("role")
}

// Before Create
func (m *Role) BeforeCreate(scope *gorm.Scope) error {
	m.CreatedAt = time.Now()
	m.UpdatedAt = time.Now()
	return nil
}

// Before Update
func (m *Role) BeforeUpdate(scope *gorm.Scope) error {
	m.UpdatedAt = time.Now()
	return nil
}

// Delete roles and associated data
func (Role) Delete(roleids []uint64) error {
	tx := db.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Where("id in (?)", roleids).Delete(&Role{}).Error; err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Where("role_id in (?)", roleids).Delete(&RoleMenu{}).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}
