package sys

import (
	"time"

	"gitlab.com/fuadhs/goapp/internal/pkg/models/basemodel"
	"gitlab.com/fuadhs/goapp/internal/pkg/models/db"

	"github.com/jinzhu/gorm"
)

// Menu
type Menu struct {
	basemodel.Model
	Status      uint8  `gorm:"column:status;type:tinyint(1);not null;" json:"status" form:"status"`             // Status (1: Enabled 2: Disabled)
	Memo        string `gorm:"column:memo;size:64;" json:"memo" form:"memo"`                                    // Memo
	ParentID    uint64 `gorm:"column:parent_id;not null;" json:"parent_id" form:"parent_id"`                    // Root ID
	URL         string `gorm:"column:url;size:72;" json:"url" form:"url"`                                       // URL
	Name        string `gorm:"column:name;size:32;not null;" json:"name" form:"name"`                           // Name
	Sequence    int    `gorm:"column:sequence;not null;" json:"sequence" form:"sequence"`                       // sequence
	MenuType    uint8  `gorm:"column:menu_type;type:tinyint(1);not null;" json:"menu_type" form:"menu_type"`    // Menu type 1 module 2 menu 3 operation
	Code        string `gorm:"column:code;size:32;not null;unique_index:uk_menu_code;" json:"code" form:"code"` // Menu Code
	Icon        string `gorm:"column:icon;size:32;" json:"icon" form:"icon"`                                    // icon
	OperateType string `gorm:"column:operate_type;size:32;not null;" json:"operate_type" form:"operate_type"`   // Action none/add/del/view/update
}

// Table Name
func (Menu) TableName() string {
	return TableName("menu")
}

// Before Create
func (m *Menu) BeforeCreate(scope *gorm.Scope) error {
	m.CreatedAt = time.Now()
	m.UpdatedAt = time.Now()
	return nil
}

// Before Update
func (m *Menu) BeforeUpdate(scope *gorm.Scope) error {
	m.UpdatedAt = time.Now()
	return nil
}

// Get the list of operations that the menu has permission
func (Menu) GetMenuButton(adminsid uint64, menuCode string, btns *[]string) (err error) {
	sql := `select operate_type from tb_sys_menu
	      where id in (
					select menu_id from tb_sys_role_menu where 
					menu_id in (select id from tb_sys_menu where parent_id in (select id from tb_sys_menu where code=?))
					and role_id in (select role_id from tb_sys_admins_role where admins_id=?)
				)`
	err = db.DB.Raw(sql, menuCode, adminsid).Pluck("operate_type", btns).Error
	return
}

// Get all menus under administrator privileges
func (Menu) GetMenuByAdminsid(adminsid uint64, menus *[]Menu) (err error) {
	sql := `select * from tb_sys_menu
	      where id in (
					select menu_id from tb_sys_role_menu where 
				  role_id in (select role_id from tb_sys_admins_role where admins_id=?)
				)`
	err = db.DB.Raw(sql, adminsid).Find(menus).Error
	return
}

// Delete menu and associated data
func (Menu) Delete(menuids []uint64) error {
	tx := db.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		tx.Rollback()
		return err
	}
	for _, menuid := range menuids {
		if err := deleteMenuRecurve(tx, menuid); err != nil {
			tx.Rollback()
			return err
		}
	}
	if err := tx.Where("menu_id in (?)", menuids).Delete(&RoleMenu{}).Error; err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Where("id in (?)", menuids).Delete(&Menu{}).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}

func deleteMenuRecurve(db *gorm.DB, parentID uint64) error {
	where := &Menu{}
	where.ParentID = parentID
	var menus []Menu
	dbslect := db.Where(&where)
	if err := dbslect.Find(&menus).Error; err != nil {
		return err
	}
	for _, menu := range menus {
		if err := db.Where("menu_id = ?", menu.ID).Delete(&RoleMenu{}).Error; err != nil {
			return err
		}
		if err := deleteMenuRecurve(db, menu.ID); err != nil {
			return err
		}
	}
	if err := dbslect.Delete(&Menu{}).Error; err != nil {
		return err
	}
	return nil
}
