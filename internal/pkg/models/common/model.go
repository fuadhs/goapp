package common

// pagination condition
type PageWhereOrder struct {
	Order string
	Where string
	Value []interface{}
}