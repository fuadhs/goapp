package mast

import (
	"time"

	"gitlab.com/fuadhs/goapp/internal/pkg/models/basemodel"

	"github.com/jinzhu/gorm"
)

type JenisSurat struct {
	basemodel.Model
	ID        uint64 `gorm:"column:id;not null;primary_key;autoincrement;unique_index;" json:"id"`
	Kode      int    `gorm:"column:kode;unique;not null;" json:"kode" form:"kode"`
	JeniSurat string `gorm:"column:jenis_surat;unique;not null;" json:"jenis_surat" form:"jenis_surat"`
}

// Table Name
func (JenisSurat) TableName() string {
	return TableName("jenis_surat")
}

func (j *JenisSurat) BeforeCreate(scope *gorm.Scope) error {
	j.CreatedAt = time.Now()
	j.UpdatedAt = time.Now()
	return nil
}

func (j *JenisSurat) BeforeUpdate(scope *gorm.Scope) error {
	j.UpdatedAt = time.Now()
	return nil
}
