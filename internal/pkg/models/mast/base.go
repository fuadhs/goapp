package mast

import (
	"fmt"

	"gitlab.com/fuadhs/goapp/internal/pkg/models/basemodel"
)

func TableName(name string) string {
	return fmt.Sprintf("%s%s%s", basemodel.GetTablePrefix(), "m_", name)
}
