package mast

import (
	"time"

	"gitlab.com/fuadhs/goapp/internal/pkg/models/basemodel"

	"github.com/jinzhu/gorm"
)

type Bidang struct {
	basemodel.Model
	ID   uint   `gorm:"column:id;not null:primary_key;autoIncrement;unique_index;" json:"id"`
	Kode string `gorm:"column:kode;not null;unique" json:"kode" form:"kode"`
	Nama string `gorm:"column:nama;not null;unique" json:"nama" form:"nama"`
}

// Table Name
func (Bidang) TableName() string {
	return TableName("bidang")
}

func (b *Bidang) BeforeCreate(scope *gorm.Scope) error {
	b.CreatedAt = time.Now()
	b.UpdatedAt = time.Now()
	return nil
}

func (b *Bidang) BeforeUpdate(scope *gorm.Scope) error {
	b.UpdatedAt = time.Now()
	return nil
}
